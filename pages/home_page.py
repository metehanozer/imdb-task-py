from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class HomePage(BasePage):

    url = 'https://www.imdb.com/'

    @property
    def search_input(self):
        locator = (By.CSS_SELECTOR, "input#suggestion-search")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def search_button(self):
        locator = (By.CSS_SELECTOR, "button#suggestion-search-button")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
