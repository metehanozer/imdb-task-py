from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class GenrePage(BasePage):

    url = 'https://www.imdb.com/feature/genre'

    @property
    def genre_list(self):
        locator = (By.XPATH, "/html//div[@id='main']/div[6]//div[@class='widget_nested']//div//div//div//div//div//a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
