from selenium.webdriver.common.by import By

from utils.base_element import BaseElement
from utils.base_page import BasePage


class MoviePage(BasePage):

    url = None

    @property
    def title(self):
        locator = (By.CSS_SELECTOR, ".title_block h1")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def year(self):
        locator = (By.CSS_SELECTOR, "span#titleYear > a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def rating(self):
        locator = (By.CSS_SELECTOR, ".imdbRating > .ratingValue > strong > span")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def genre_list(self):
        locator = (By.XPATH, "//div[@class='subtext']//a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])
